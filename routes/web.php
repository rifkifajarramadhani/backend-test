<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GiftController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['guest'])->group(function() {
    //login
    Route::get('/login', [LoginController::class, 'index'])->name('login');
    Route::post('/login', [LoginController::class, 'authenticate']);
    Route::post('/users', [UserController::class, 'create']);
});

Route::middleware(['auth'])->group(function() {
    //logout
    Route::post('/logout', [LoginController::class, 'logout']);
    
    ROute::middleware(['admin'])->group(function() {
        //users
        Route::get('/users', [UserController::class, 'index']);
        Route::get('/users/{user}', [UserController::class, 'show']);

        //gifts admin
        Route::post('/gifts', [GiftController::class, 'store']);
        Route::post('/gifts/stock/{gift}', [GiftController::class, 'storeStock']);
        Route::put('/gifts/{gift}', [GiftController::class, 'update']);
        Route::patch('/gifts/{gift}', [GiftController::class, 'updateDescription']);
        Route::delete('/gifts/{gift}', [GiftController::class, 'destroy']);
    });
    Route::put('/users/{user}', [UserController::class, 'update']);

    //gifts
    Route::get('/gifts', [GiftController::class, 'index']);
    Route::get('/gifts/{gift}', [GiftController::class, 'show']);
    Route::post('/gifts/{gift}/redeem', [GiftController::class, 'redeem']);
    Route::post('/gifts/{redeem}/rating', [GiftController::class, 'rating']);
});