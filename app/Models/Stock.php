<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Stock extends Model
{
    use HasFactory;

    protected $fillable = [
        'year',
        'month',
        'gift_id',
        'stock'
    ];

    public function gift()
    {
        return $this->belongsTo(Gift::class);
    }

    public static function getStock($today, $giftId)
    {
        $year = Carbon::parse($today)->format('Y');
        $month = Carbon::parse($today)->format('m');

        $stock = Gift::leftJoin('stocks', 'gifts.id', 'stocks.gift_id')
        ->leftJoin('redeems', 'gifts.id', 'redeems.gift_id')
        ->where('stocks.year', $year)
        ->where('stocks.month', $month)
        ->where('gifts.id', $giftId)
        ->selectRaw("
            gifts.id,
            gifts.name,
            stocks.stock initial_stock,
            stocks.stock - ifnull((select sum(redeems.qty) from redeems where gift_id = gifts.id and year(date) = stocks.year and month(date) = stocks.month), 0) stock
        ")
        ->groupBy('gifts.id')
        ->first();

        return $stock;
    }
}
