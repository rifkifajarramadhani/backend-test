<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    use HasFactory;
    
    protected $guarded = ['id'];

    public function redeem()
    {
        return $this->hasMany(Redeem::class);
    }

    public function stock()
    {
        return $this->hasMany(Stock::class);
    }
}
