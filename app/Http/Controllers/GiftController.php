<?php

namespace App\Http\Controllers;

use App\Models\Gift;
use App\Models\Stock;
use App\Models\Redeem;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;

class GiftController extends Controller
{
    public function index(Request $request)
    {
        try {
            $year = Carbon::now()->format('Y');
            $month = Carbon::now()->format('m');
            
            $gifts = Gift::leftJoin('redeems', 'gifts.id', 'redeems.gift_id')
            ->leftJoin('stocks', 'gifts.id', 'stocks.gift_id')
            ->select('gifts.*')
            ->selectRaw("
                floor((avg(rating)) * 2 + 0.5) / 2 rating,
                stocks.stock - ifnull((select sum(redeems.qty) from redeems where gift_id = gifts.id and year(date) = stocks.year and month(date) = stocks.month), 0) stock
            ")
            ->where('stocks.year', $year)
            ->where('stocks.month', $month)
            ->groupBy('gifts.id');

            $isSort = $request->sort;
            if($isSort) {
                if($request->sort == 'asc') {
                    $gifts->orderBy('created_at', 'asc')->orderBy('rating', 'asc');
                } else {
                    $gifts->orderBy('created_at', 'desc')->orderBy('rating', 'desc');
                }
            }

            $isRating = $request->rating;
            if($isRating) {
                $gifts->havingRaw("rating >= $isRating");
            }

            $isStockAvailable = $request->is_stock_available;
            if($isStockAvailable) {
                $gifts->havingRaw("stock > 0");
            }

            $gifts = $gifts->paginate(10);

            $message = 'Success get gifts';
            
            return response()->json([
                'result' => true,
                'message' => $message,
                'gifts' => $gifts
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function show(Gift $gift)
    {
        try {
            $giftId = $gift->id;
            $giftRating = Gift::join('redeems', 'gifts.id', 'redeems.gift_id')
            ->selectRaw("floor((avg(rating)) * 2 + 0.5) / 2 rating")
            ->where('gifts.id', $giftId)
            ->first();
            $gift->rating = $giftRating->rating;

            $message = 'Success get gift data';

            return response()->json([
                'result' => true,
                'message' => $message,
                'gift' => $gift
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'slug' => 'required|unique:gifts',
                'price' => 'required',
                'description' => 'required'
            ]);

            if($validator->fails()) {
                $message = 'Please complete data';
                $error = $validator->messages();

                return response()->json([
                    'result' => false,
                    'message' => $message,
                    'error' => $error
                ]);
            }

            Gift::create($request->all());

            $message = 'Success input new gift';

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            DB::rollback();
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function storeStock(Request $request, Gift $gift)
    {
        try {
            $giftId = $gift->id;
            $year = Carbon::now()->format('Y');
            $month = Carbon::now()->format('m');

            $stockCheck = Gift::join('stocks', 'gifts.id', 'stocks.gift_id')
            ->where('year', $year)
            ->where('month', $month)
            ->where('gift_id', $giftId)
            ->first();
            if(isset($stockCheck)) {
                $message = 'Stock already inputed';

                return response()->json([
                    'result' => false,
                    'message' => $message
                ]);
            }

            Stock::create([
                'year' => $year,
                'month' => $month,
                'gift_id' => $giftId,
                'stock' => 50
            ]);

            $message = 'Success insert stock';

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
    
    public function update(Request $request, Gift $gift)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'slug' => "required|unique:gifts,slug,$gift->id",
                'price' => 'required',
                'description' => 'required'
            ]);

            if($validator->fails()) {
                $message = 'Please complete or check data';
                $error = $validator->messages();

                return response()->json([
                    'result' => false,
                    'message' => $message,
                    'error' => $error
                ]);
            }

            $giftId = $gift->id;
            Gift::where('id', $giftId)->update($request->all());

            $message = 'Success update gift';

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
    
    public function updateDescription(Request $request, Gift $gift)
    {
        try {
            $validator = Validator::make($request->all(), [
                'description' => 'required'
            ]);

            if($validator->fails()) {
                $message = 'Please complete or check data';
                $error = $validator->messages();

                return response()->json([
                    'result' => false,
                    'message' => $message,
                    'error' => $error
                ]);
            }

            $description = $request->description;
            Gift::where('id', $gift->id)->update(['description' => $description]);

            $message = 'Success update gift description';

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function destroy(Gift $gift)
    {
        try {
            $giftId = $gift->id;
            Gift::destroy($giftId);

            $message = 'Success delete gift';

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function redeem(Request $request, Gift $gift)
    {
        try {
            $validator = Validator::make($request->all(), [
                'qty' => 'required|min:1'
            ]);

            if($validator->fails()) {
                $message = 'Please complete or check data';
                $error = $validator->messages();

                return response()->json([
                    'result' => false,
                    'message' => $message,
                    'error' => $error
                ]);
            }

            $qty = $request->qty;
            $userId = auth()->user()->id;
            $giftId = $gift->id;
            $giftName = $gift->name;
            $today = Carbon::now()->format('Y-m-d');

            $stock = Stock::getStock($today, $giftId);
            if($stock->stock < $qty) {
                $message = "Insufficient stock, stock available for item $giftName is $stock->stock";

                return response()->json([
                    'result' => false,
                    'message' => $message
                ]);
            }

            Redeem::create([
                'user_id' => $userId,
                'gift_id' => $giftId,
                'qty' => $qty,
                'date' => $today
            ]);

            $message = 'Success redeem gift';

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function rating(Request $request, Redeem $redeem)
    {
        try {
            $validator = Validator::make($request->all(), [
                'rating' => 'required|numeric|min:1|max:5'
            ]);

            if($validator->fails()) {
                $message = 'Please complete or check data';
                $error = $validator->messages();

                return response()->json([
                    'result' => false,
                    'message' => $message,
                    'error' => $error
                ]);
            }
            
            $redeemId = $redeem->id;
            $redeemUserId = $redeem->user_id;
            $userId = auth()->user()->id;
            $rating = $request->rating;
            if($redeemUserId != $userId) {
                $message = 'Unauthorized user';

                return response()->json([
                    'result' => false,
                    'message' => $message
                ]);
            }

            Redeem::where('id', $redeemId)
            ->where('user_id', $userId)
            ->update([
                'rating' => $rating
            ]);

            $message = 'Success submit rating';

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
}
