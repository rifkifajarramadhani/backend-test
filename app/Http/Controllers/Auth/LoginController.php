<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    public function index()
    {
        try {
            $message = 'This should be login page';
    
            $data = [
                'result' => true,
                'message' => $message
            ];
    
            return response()->json($data);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            $data = [
                'result' => false,
                'message' => 'Something went wrong'
            ];

            return response()->json($data);
        }
    }

    public function authenticate(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if($validator->fails()) {
                $message = 'Please complete data';
                $error = $validator->messages();

                $data = [
                    'result' => false,
                    'message' => $message,
                    'error' => $error
                ];

                return response()->json($data);
            }
            
            $credentials = $request->all();

            $isAuthenticated = Auth::attempt($credentials);
    
            if($isAuthenticated) {
                $message = 'Login Success';
                
                $data = [
                    'result' => true,
                    'message' => $message
                ];
                
                $request->session()->regenerate();

                return response()->json($data);
            } else {
                $message = 'Login Failed';

                $data = [
                    'result' => false,
                    'message' => $message
                ];

                return response()->json($data);
            }
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            $data = [
                'result' => false,
                'message' => 'Something went wrong'
            ];

            return response()->json($data);
        }
    }

    public function logout(Request $request)
    {
        try {
            Auth::logout();

            $request->session()->invalidate();
            $request->session()->regenerateTOken();

            $message = 'Logout Success';
            
            $data = [
                'result' => true,
                'message' => $message
            ];

            return response()->json($data);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            $data = [
                'result' => false,
                'message' => 'Something went wrong'
            ];

            return response()->json($data);
        }
    }
}
