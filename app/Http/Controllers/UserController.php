<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function index()
    {
        try {
            $users = User::get();

            $message = 'Success get users data';

            return response()->json([
                'result' => true,
                'message' => $message,
                'users' => $users
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function show(User $user)
    {
        try {
            $message = "Success get user $user->name data";

            return response()->json([
                'result' => true,
                'message' => $message,
                'user' => $user
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|unique:users',
                'password' => 'required'
            ]);

            if($validator->fails()) {
                $message = 'Please complete data';
                $error = $validator->messages();

                return response()->json([
                    'result' => false,
                    'message' => $message,
                    'error' => $error
                ]);
            }

            $name = $request->name;
            $email = $request->email;
            $password = bcrypt($request->password);

            User::create([
                'name' => $name,
                'email' => $email,
                'password' => $password
            ]);

            $message = "Success register data";

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
    
    public function update(Request $request, User $user)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => "required|unique:users,email,$user->id",
                'password' => 'required'
            ]);

            if($validator->fails()) {
                $message = 'Please complete or check data';
                $error = $validator->messages();

                return response()->json([
                    'result' => false,
                    'message' => $message,
                    'error' => $error
                ]);
            }

            $userId = $user->id;
            $updateUserId = auth()->user()->id;
            if($userId != $updateUserId) {
                $message = 'Unauthorized user';

                return response()->json([
                    'result' => false,
                    'message' => $message
                ]);
            }

            $name = $request->name;
            $email = $request->email;
            $password = bcrypt($request->password);

            User::where('id', $userId)->update([
                'name' => $name,
                'email' => $email,
                'password' => $password
            ]);

            $message = 'Success update user';

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
    
    public function destroy(User $user)
    {
        try {
            $userId = $user->id;
            User::destroy($userId);

            $message = 'Success delete gift';

            return response()->json([
                'result' => true,
                'message' => $message
            ]);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            $data = [
                'result' => false,
                'message' => 'Something went wrong'
            ];

            return response()->json($data);
        }
    }
}
