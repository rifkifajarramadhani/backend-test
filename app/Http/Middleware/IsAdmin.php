<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $isAdmin = auth()->user()->is_admin;
        if(!$isAdmin) {
            $message = 'Unauthorized user';

            return response()->json([
                'result' => false,
                'message' => $message
            ]);
        }

        return $next($request);
    }
}
