This is a PHP backend app with Laravel framework (version 8).

## Getting Started

The requirements for this project:
-	PHP 7.4 +
-	composer 2.0

After clone the project into local envireonment, install / update vendor:

```bash
composer install
# or
composer update
```

Then, create the .env from .env.example file and set the database to rolling_glory and generate the app key:

```bash
php artisan key:generate
```


And clear the app cache:

```bash
php artisan config:cache
```


For the testing via POSTMAN, I disabling the CSRF token verification. You can check at App\Http\Middleware\VerifyCsrfToken.php, at the $except variable there are some routes that I decided to disable the token verification.

```
protected $except = [
        '/login',
        '/logout',
        '/users',
        '/users/*',
        '/gifts',
        '/gifts/*',
    ];
```

I have included the DB schema and data file, but you can use the Faker and Seeder method to test with bigger data sample.
You just have to run migration + seeder:

```bash
php artisan migrate:fresh --seed
```

Add into .env file:

```
FAKER_LOCALE=id_ID
```

This command will do a fresh migration (drop all the tables an do a migation) and then fill the database with the custom seeder.

Or if you choose to use the DB schema file, just import the DB schema file.


After do all the step, your app is ready to be tested via POSTMAN (docs).
