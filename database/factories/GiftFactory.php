<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class GiftFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(mt_rand(1, 3)),
            'slug' => $this->faker->slug(),
            'price' => $this->faker->numberBetween(1000000, 5000000),
            'description' => $this->faker->paragraph(mt_rand(3, 5))
        ];
    }
}
