<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class RedeemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => mt_rand(1, 5),
            'gift_id' => mt_rand(1, 5),
            'qty' => mt_rand(1, 3),
            'date' => '2021-12-' . mt_rand(1, 8),
            'rating' => mt_rand(1, 5)
        ];
    }
}
