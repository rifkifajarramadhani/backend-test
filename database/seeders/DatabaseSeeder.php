<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Gift;
use App\Models\Redeem;
use App\Models\Stock;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(5)->create();
        Gift::factory(5)->create();
        Redeem::factory(10)->create();
        Stock::factory(5)->create();
    }
}
